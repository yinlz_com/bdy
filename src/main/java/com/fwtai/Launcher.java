package com.fwtai;

import com.fwtai.service.ApiService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class Launcher{

  private final ApiService apiService;

  public static void main(final String[] args) {
    SpringApplication.run(Launcher.class, args);
    System.out.println("http://127.0.0.1:5512/");
  }

  public Launcher(final ApiService apiService){
    this.apiService = apiService;
  }

  // 每月的1号和16号的2点34分0秒执行刷新token,因为token的有效期是30天,所以要提前刷新token -> (0 34 2 1,16 ? *) ok
  // 每月的1号16号22点50分69秒执行(59 50 22 1,16 ? *); ok
  // 每月的1、5、16号的22点38分0秒执行(0 38 22 1,5,16 ? *); ok
  // 每月的1,5,16号的22点42分43秒执行(43 42 22 1,5,16 ? *) ok
  @Scheduled(cron = "0 15 3 14,28 * *")//每月14号,28号的3:15执行一次
  public void refreshToken(){
    apiService.refreshToken(39630237);//应用名称:文件备份
  }
}