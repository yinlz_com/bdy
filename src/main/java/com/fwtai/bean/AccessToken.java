package com.fwtai.bean;

public final class AccessToken{

  private String clientId;
  private String clientSecret;

  public String getClientId(){
    return clientId;
  }

  public void setClientId(final String clientId){
    this.clientId = clientId;
  }

  public String getClientSecret(){
    return clientSecret;
  }

  public void setClientSecret(final String clientSecret){
    this.clientSecret = clientSecret;
  }
}