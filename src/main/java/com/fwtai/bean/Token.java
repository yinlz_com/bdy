package com.fwtai.bean;

public final class Token{

  private Integer kid;
  private String access_token;
  private String refresh_token;
  private Integer expires_in;

  public Integer getKid(){
    return kid;
  }

  public void setKid(final Integer kid){
    this.kid = kid;
  }

  public String getAccess_token(){
    return access_token;
  }

  public void setAccess_token(final String access_token){
    this.access_token = access_token;
  }

  public String getRefresh_token(){
    return refresh_token;
  }

  public void setRefresh_token(final String refresh_token){
    this.refresh_token = refresh_token;
  }

  public Integer getExpires_in(){
    return expires_in;
  }

  public void setExpires_in(final Integer expires_in){
    this.expires_in = expires_in;
  }
}