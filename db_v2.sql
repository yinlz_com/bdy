/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.1.66
 Source Server Type    : MySQL
 Source Server Version : 80037 (8.0.37)
 Source Host           : 192.168.1.66:3306
 Source Schema         : baiduyunpan

 Target Server Type    : MySQL
 Target Server Version : 80037 (8.0.37)
 File Encoding         : 65001

 Date: 01/06/2024 16:49:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `kid` int NOT NULL COMMENT '应用主键',
  `access_token` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问凭证',
  `refresh_token` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '刷新凭证',
  `expires_in` int NOT NULL DEFAULT 2592000 COMMENT '失效秒数（30天）',
  `refresh_time` int UNSIGNED NOT NULL COMMENT '更新时刻（刷新时间）',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '我的应用/应用详情/应用名称',
  `client_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'client_id（appKey）',
  `client_secret` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'client_secret（secretKey）',
  `folder` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件上传指定的目录',
  PRIMARY KEY (`kid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES (39630237, '121.f105fc451dd63016509587431c9925cd.YBAzCZim4acOe23V43Iw3xeBxPn2pUQ2HYlLQxw._AO_0g', '122.6040d7d33bfb1b7b130bd9b8393935e6.YsmMMVFRm0zs3ZCIXXTcgo8O0GkU2EJGuL3vGcT.WbBcAg', 2592000, 1717231634, '文件备份', 'j61RTI6qcDDpDAyzQjOlLQV7YPGWqTir', 'ZghjMeHyj50V89NeGwZBQUy2ugOhseqE', '/apps/files/');

SET FOREIGN_KEY_CHECKS = 1;
