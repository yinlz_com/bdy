/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.1.121_frp.tfkj.top
 Source Server Type    : MySQL
 Source Server Version : 80035 (8.0.35)
 Source Host           : frp.tfkj.top:12567
 Source Schema         : bdy

 Target Server Type    : MySQL
 Target Server Version : 80035 (8.0.35)
 File Encoding         : 65001

 Date: 04/05/2024 22:39:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `kid` int NOT NULL,
  `access_token` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `refresh_token` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expires_in` int NOT NULL DEFAULT 2592000,
  PRIMARY KEY (`kid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39630238 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` (`kid`, `access_token`, `refresh_token`, `expires_in`) VALUES (39630237, '121.d6ebea8a40b5487500ad2fb284bc6226.Y7MF79yXNbhki24fsoYEG2_hDDpCMBaZ1uBNuap.4kjaYQ', '122.0844d59d4b488ded0c0fec76aa1de8da.YanJmbn6VX-KwphbKrJcdghZSYzQDlka9r0Dy0A.r0Ws7A', 2592000);
SET FOREIGN_KEY_CHECKS = 1;
